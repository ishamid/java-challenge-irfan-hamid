package ist.challenge.Irfan.Hamid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrfanHamidApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrfanHamidApplication.class, args);
	}

}
