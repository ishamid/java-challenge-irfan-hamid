package ist.challenge.Irfan.Hamid.Controller;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import ist.challenge.Irfan.Hamid.Model.User;
import ist.challenge.Irfan.Hamid.Repository.UserRepository;

public class UserController {
  @Autowired
  UserRepository userRepository;
  User userModel;

  @GetMapping("/list")
  public ResponseEntity<List<User>> findAll(@RequestParam(required = false) String username) {
      try {
          List<User> users = new ArrayList<User>();

          if (username == null)
              userRepository.findAll().forEach(users::add);
          else
              userRepository.findByUsername(username).forEach(users::add);

          if (users.isEmpty()) {
              return new ResponseEntity<>(HttpStatus.NO_CONTENT);
          }

          return new ResponseEntity<>(users, HttpStatus.OK);
      } catch (Exception e) {
          return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
  }

  // login
  // public interface UserRepository extends JpaRepository<User, Long> {
    //   @Query("SELECT u FROM User u WHERE u.username = ?1")
    //   public User findByUsername(String username); 
  // }

  
  @PostMapping("/register")
  public ResponseEntity<User> create(
          @RequestBody User users) {
      try {
          User newUser = new User();
          // newUser.setId(UUID.randomUUID().toString());
          newUser.setUsername(users.getUsername());
          newUser.setPassword(users.getPassword());
          return new ResponseEntity<>(userRepository.save(newUser), HttpStatus.CREATED);
      } catch (Exception e) {
          return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
  }

}
