package ist.challenge.Irfan.Hamid.Repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import ist.challenge.Irfan.Hamid.Model.User;
public interface UserRepository extends JpaRepository <User, Long>{
    List<User> findByUsername(String username);
}
